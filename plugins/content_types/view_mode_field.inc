<?php
/**
 * @file
 * Handle rendering entities based on view mode fields.
 */

$plugin = array(
  'title' => t('View mode field'),
  'defaults' => array('delta' => 0),
  'content type' => 'view_mode_field_view_mode_field_content_type_content_type',
);

function view_mode_field_view_mode_field_content_type_content_type($subtype) {
  $types = view_mode_field_view_mode_field_content_type_content_types();
  if (isset($types[$subtype])) {
    return $types[$subtype];
  }
}

/**
 * Return all field content types available.
 */
function view_mode_field_view_mode_field_content_type_content_types() {
  $types = &drupal_static(__FUNCTION__, array());
  if (!empty($types)) {
    return $types;
  }
  $fields = field_info_fields();
  $view_mode_fields = array();
  $types = array();
  foreach ($fields as $name => $field) {
    if ($field['type'] == 'view_mode_field_view_mode') {
      $view_mode_fields[$name] = $field;
      // Get the entity type.
      $entity_type = $field['settings']['entity_type'];
      $instances = field_read_instances(array('field_name' => $name));
      foreach ($instances as $instance) {
        $key = $instance['entity_type'] . ':' . $name;
        if (!isset($types[$key])) {
          $types[$key] = array(
            'category' => t(ucfirst($entity_type)),
            'title' => t('View mode field: @name', array('@name' => $name)),
            'description' => t('View mode field.'),
            'defaults' => array('delta' => 0),
            'required context' => array(
              new ctools_context_required(t(ucfirst($entity_type)), 'entity:' . $entity_type),
              new ctools_context_required(t(ucfirst($instance['entity_type'])), 'entity:' . $instance['entity_type']),
            ),
          );
        }
      }
    }
  }
  return $types;
}

/**
 * Empty form so we can have the default override title.
 */
function view_mode_field_view_mode_field_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['delta'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $conf['delta'],
    '#title' => t('Delta'),
  );
  return $form;
}

function view_mode_field_view_mode_field_content_type_edit_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['delta'])) {
    form_set_error('delta', t('The delta value must be numeric.'));
  }
}

/**
 * Submit handler for the custom type settings form.
 */
function view_mode_field_view_mode_field_content_type_edit_form_submit(&$form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Render the custom content type.
 */
function view_mode_field_view_mode_field_content_type_render($subtype, $conf, $panel_args, $contexts) {
  if (empty($contexts)) {
    return FALSE;
  }
  $render_entity = array_shift($contexts);
  $field_entity = array_shift($contexts);
  list(,$field_name) = explode(':', $subtype);
  list(,$render_entity_type) = explode(':', $render_entity->plugin);
  list(,$field_entity_type) = explode(':', $field_entity->plugin);
  $items = field_get_items($field_entity_type, $field_entity->data, $field_name);
  if (isset($items[$conf['delta']]['view_mode'])) {
    $view_mode = $items[$conf['delta']]['view_mode'];
    $block = new stdClass();
    $block->module = 'view_mode_field';
    $block->content = entity_view($render_entity_type, array($render_entity->data), $view_mode);
    return $block;
  }
}

/**
* Returns the administrative title for a type.
*/
function view_mode_field_view_mode_field_content_type_admin_title($subtype, $conf, $contexts) {
  $render_entity = array_shift($contexts);
  $field_entity = array_shift($contexts);
  return t('Rendered entity @render_entity with view mode from @subtype',
    array('@render_entity' => $render_entity->identifier, '@subtype' => $subtype));
}

