<?php
/**
 * @file
 */

/**
 * Implements hook_field_info().
 */
function view_mode_field_field_info() {
  return array(
    'view_mode_field_view_mode' => array(
      'label' => t('View mode'),
      'description' => t('Stores a reference to a particular view mode.'),
      'settings' => array('entity_type' => NULL),
      'default_widget' => 'options_select',
      'default_formatter' => 'view_mode_field_plain',
      'default_token_formatter' => 'view_mode_field_plain',
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function view_mode_field_field_settings_form($field, $instance) {
  $settings = $field['settings'];
  $entity_types = entity_get_info();
  $options = array();
  foreach ($entity_types as $entity_type => $info) {
    $options[$entity_type] = $info['label'];
  }
  $form['entity_type'] = array(
    '#type' => 'select',
    '#title' => t('Entity type'),
    '#required' => TRUE,
    '#default_value' => $settings['entity_type'],
    '#options' => $options,
  );
  return $form;
}

/**
 * Implements hook_options_list().
 */
function view_mode_field_options_list($field) {
  $entity_info = entity_get_info($field['settings']['entity_type']);
  $options = array();
  foreach ($entity_info['view modes'] as $view_mode => $info) {
    $options[$view_mode] = $info['label'];
  }
  return $options;
}

/**
 * Implements hook_field_widget_info_alter().
 */
function view_mode_field_field_widget_info_alter(&$info) {
  $info['options_select']['field types'][] = 'view_mode_field_view_mode';
  $info['options_buttons']['field types'][] = 'view_mode_field_view_mode';
}


/**
 * Implements hook_field_presave().
 */
function view_mode_field_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => $item) {
    $items[$delta]['safe'] = check_plain($item['view_mode']);
  }
}

/**
 * Implements hook_field_is_empty().
 */
function view_mode_field_field_is_empty($item, $field) {
  if (empty($item['view_mode'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_formatter_info().
 */
function view_mode_field_field_formatter_info() {
  return array(
    'view_mode_field_plain' => array(
      'label' => t('Plain text'),
      'field types' => array('view_mode_field_view_mode'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function view_mode_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  foreach ($items as $delta => $item) {
    $element[$delta] = array('#markup' => check_plain($item['view_mode']));
  }
  return $element;
}

/**
 * Implements of hook_ctools_plugin_directory().
 */
function view_mode_field_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools') {
    return 'plugins/' . $plugin;
  }
}
